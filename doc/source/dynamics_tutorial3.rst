Computational Lab: Dynamics 3
=============================

This is the tutorial material for Computational Lab 3 in the Dynamics
core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpcdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about weak and strong convergence for 
numerical methods for stochastic differential equations

.. container:: tasks  

   After merging the latest version of the mpecdt repository, you will
   find a file to the mpecdt/src directory called `ergodic.py`, in
   which you should add all of the tasks from this tutorial.
   Exercises 2 and 3 will be assessed as part of the coursework for
   this project. Solutions to the exercises should execute as part of
   the script; use print statements to answer any questions in the
   exercises.  Please email `colin.cotter@imperial.ac.uk` the git
   revision for your repository in the form of a SHA (just type `git
   log` to get the list of SHAs for each revision) before midnight GMT
   on 14th November 2014. The marks will be allocated as follows: 50\%
   for Exercise 2, 30\% for Exercise 3, 20\% for Python coding style
   (use of functions, code reuse, efficiency, readability etc.).

We are going to continue to use the Lorenz system as an example, given by

.. math::

   \begin{eqnarray}
   \dot{x} &= \sigma (y-x)\\
   \dot{y} &= x(\rho-z) - y  \\ 
   \dot{z} &= xy - \beta z
   \end{eqnarray}

and parameter values :math:`\sigma = 10,\,\rho = 28`, and :math:`\beta = 8/3`.

However, we are now going to take a more probabilistic view of the
dynamics. For a dynamical system 

.. math::
   \dot{x} = f(x),

say that our initial condition is not known precisely, but is assumed
to be a random variable with PDF :math:`\pi_0(x)`. At later times, the
variable evolves according to the dynamical system, and has PDF
:math:`\pi_t(x)` at time :math:`t`, which satisfies the Liouville
equation,

.. math::
   \frac{\partial}{\partial t}\pi_t + \sum_{i=1}^d\frac{\partial}{\partial x_i}(f_i\pi_t) = 0,

test
where :math:`(x_1,x_2,\ldots,x_d)` are the components of :math:`x`,
which is :math:`d` -dimensional, and :math:`(f_1,f_2,\ldots,f_d)` are
the components of :math:`f(x)`. This equation allows us to do
probabilistic forecasting by propagating our uncertainty in the form
of a PDF forwards in time to provide probabilities of the future state
of the system.

The cost of solving this PDE using a grid-based method (such as finite
difference or finite element) grows exponentially with the dimension
:math:`d`. An alternative is to use a Monte Carlo approximation. We 
replace our initial PDF :math:`\pi_0(x)` by the empirical distribution

.. math::
   \pi(x) = \frac{1}{M}\sum_{i=1}^M \delta(x-X_i(0)),

where the variables :math:`\{X_i(0)\}_{i=1}^M` are independent,
individually distributed random variables drawn from
:math:`\pi_0`. These variables are referred to as "particles" or
"ensemble members".

At later times, the approximation satisfies

.. math::
   \pi(x) = \frac{1}{M}\sum_{i=1}^M \delta(x-X_i(t)),

where each of the variables :math:`\{X_i(0)\}_{i=1}^M` are solutions
of the dynamical system. The only source of error in this
approximation is the initialisation, and timestepping error in solving
the dynamical system.

Exercise 1
----------

.. container:: tasks  

   The file `ergodic.py` implements the Monte Carlo method to solve the
   probabilistic forecasting problem for the Lorenz equation, using an
   initial condition which is the normal distribution with mean zero
   and covariance matrix equal to the identity multiplied by 0.1, with
   :math:`M=100`.  Modify the code to visualise this solution at
   :math:`t=0`, :math:`t=0.5`, :math:`t=1`, :math:`t=10`,
   :math:`t=100` by plotting the location of the ensemble members. If
   feasible increase :math:`M` to obtain a more accurate solution.

It is known that the solution of the Liouville equation for the Lorenz
system converges to an invariant measure :math:`\mu(x)` as
:math:`t\to\infty` under very general assumptions on the initial
conditions for :math:`\pi_0(x)`. The invariant measure is supported on
the Lorenz attractor. Hence, for large times, our Monte Carlo method
produces an empirical approximation to the Lorenz invariant measure.

Exercise 2
----------

The Lorenz system is known to be ergodic. This means that, for any
(sensible) function :math:`f:\mathbb{R}^3\to \mathbb{R}`, we have

.. math::
   \lim_{T\to \infty}\frac{1}{T}\int_0^T f(X(t)) dt =
   \int_{\mathbb{R}^3} f(x) d\mu(x),

where :math:`X(t)` is a solution to the Lorenz equations, and
:math:`\mu` is the invariant measure of the Lorenz equations. This
means that time averages of functions along solutions of the Lorenz
equations are equal to space averages weighted by the invariant
measure. Further, this ergodic time average can also be calculated
using the limit of a discrete sum, and we have

.. math::
   \lim_{N\to\infty}\frac{1}{N}\sum_{n=1}^N f(X^n)  =
   \int_{\mathbb{R}^3} f(x) d\mu(x).

We can approximate the spatial average using our Monte Carlo
approximation in the previous exercise.

.. math::
  \frac{1}{M} \sum_{i=1}^M f(X_i(T)),

for some large time :math:`T`.


.. container:: tasks  
	       
   Write a function to compute this spatial average, making use of the
   functions provided. The error in the Monte Carlo approximation is
   proportional to :math:`1/\sqrt{M}`. This error is random, depending
   on the random initial condition for the ensemble. Choosing a
   function `f` investigate the variance in the approximation with
   respect to this random initial condition. Include code in your
   script that demonstrates this investigation.

We can approximate the time average numerically by taking a numerical
solution :math:`\{X^n\}_{n=1}^N` with :math:`X^n = X(t^n), t^n=n\Delta
t, \quad n=1,\ldots N` for finite :math:`N`, and computing

.. math::
  \frac{1}{N} \sum_{n=1}^N f(X^n).

.. container:: tasks
	       
   Write a function to compute this time average. Choosing a function
   `f` and fixed :math:`\Delta t`, plot the value of the time average
   as a function of :math:`N`.  What do you observe about the rate of
   convergence to the average?  Demonstrate the independence of the
   time average to the initial condition.


Exercise 3
----------

If we make an analogy between predicting the Lorenz equations and
weather forecasting, then the change in the solution represents the
weather, whilst the invariant measure represents the climate. We are
now going to create some climate change! This is done by changing the
parameters :math:`\rho,\sigma,\beta` for the Lorenz equation.

.. container:: tasks

   Visualise the support of the invariant measure (either by using the
   ensemble method above or by using a single solution over a long
   time interval) for various different
   :math:`\rho,\sigma,\beta`. What happens to the shape of the
   attractor? Which parameter is the attractor shape most sensitive
   to?

.. container:: tasks
   
We would like to compute the linear response of the invariant measure,
i.e., how does the invariant measure change with small changes in the
parameters? This is difficult to observe through ensemble plots, so
we normally measure this using different functions :math:`f`.

.. container:: tasks
	       
   Choose a test function :math:`f` and plot the change in 

   .. math::
   
      \mathbb{E}_{\mu}[f(X)] = \int f(x) d\mu(x)

   under changes in :math:`\rho` around our standard value of
   :math:`\rho=28` (use the ergodic property to approximate this
   expectation from a single solution over a long time interval).
   Identify if there is a region where the response is linear in the
   variable :math:`\rho'`, where :math:`\rho=28+\rho'`. Approximate the
   partial derivative of the expectation of :math:`f` with respect to
   :math:`\rho`, using a first order difference i.e.

   .. math::
      
      \frac{\bar{f}(28+\rho',\sigma,\beta)-\bar{f}(28,\sigma,\beta)}
      {\rho'}

   for a chosen small value of :math:`\rho'`, where
   :math:`\bar{f}(\rho,\sigma,\beta)` is the expectation of
   :math:`f(X)` with respect to the invariant measure of the Lorenz
   system with parameters :math:`\rho,\sigma,\beta`.

   Perform similar calculations for the other two parameters. Repeat
   this calculation for other test functions :math:`f`. In which
   direction is :math:`\bar{f}` most sensitive in
   :math:`(\rho,\sigma,\beta)` space? Is the response always linear?
   Are there any points where the response is non-differentiable?
